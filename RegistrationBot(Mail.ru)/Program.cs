﻿using System;
using System.Drawing.Imaging;
using System.Text;
using OpenQA.Selenium;
using System.Threading;
using mevoronin.RuCaptchaNETClient;
using System.Drawing;
using System.IO;
using OpenQA.Selenium.Chrome;
using System.Net.NetworkInformation;

namespace RegistrationBot_Mail.ru_
{
    class Program
    {

        static void Main(string[] args)
        {

            //Parametrs
            #region PARAMETRS
            string API_KEY = "YOUR_API_KEY"; //your API_KEY from RUCAPTCHA.COM

            StreamReader sr = new StreamReader("c:\\CAPTCHA\\proxy.txt", Encoding.Default); //if you have PROXY FILE
            bool proxy_add = true; //if you want use PROXY - true

            string CAPTCHA_DIR = "c:\\CAPTCHA\\captcha.jpg";
            string ACCOUNTS_DIR = "c:\\CAPTCHA\\accounts.txt";
            #endregion


            IWebDriver Browser;
            string command;

            Browser = WebCreate(sr, proxy_add);

            Done("Browser opened!");

            do
            {

                Console.Write("RB >> ");

                command = Console.ReadLine();

                

                switch (command)
                {
                    case "start":
                        Browser.Navigate().GoToUrl("https://account.mail.ru/signup/simple");
                        Done("Redirected to Mail.ru!");
                        break;
                    case "create":
                        int col = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < col; i++)
                        {
                            Warning((i + 1).ToString() + " / " + col.ToString());
                            if (Browser.Url == "https://account.mail.ru/signup/simple") {

                                IWebElement FirstName = Browser.FindElement(By.Name("firstname"));
                                IWebElement LastName = Browser.FindElement(By.Name("lastname"));

                                string first, second, pass;


                                FirstName.SendKeys(first = randText(10));
                                Done("FirstName DONE!");
                                LastName.SendKeys(second = randText(10));
                                Done("LastName DONE!");

                                string mail = first + "." + second;

                                #region BirthDay_Set()
                                {
                                    Browser.FindElement(By.ClassName("b-date__day")).Click();
                                    Browser.FindElement(By.XPath(".//*[@data-text='1'] ")).Click();

                                    Browser.FindElement(By.ClassName("b-date__month")).Click();
                                    Browser.FindElement(By.XPath(".//*[@data-text='Январь'] ")).Click();

                                    Browser.FindElement(By.ClassName("b-date__year")).Click();
                                    Browser.FindElement(By.XPath(".//*[@data-text='1999'] ")).Click();
                                }
                                #endregion
                                Done("BirthDay DONE!");

                                Browser.FindElement(By.ClassName("b-radiogroup__radio")).Click();

                                Browser.FindElement(By.XPath(".//*[@data-blockid = 'email_name'] ")).SendKeys(mail);

                                Done("E-mail DONE!");

                                Browser.FindElement(By.Name("password")).SendKeys(pass = randText(15));

                                Browser.FindElement(By.Name("password_retry")).SendKeys(pass);

                                Done("Password DONE!");

                                Done("Registration Info inserted!");

                                Browser.FindElement(By.XPath(".//*[@data-blockid='btn'] ")).Click();

                                Thread.Sleep(3000);

                                IWebElement captchaImg = Browser.FindElement(By.XPath(".//*[@data-field-name='capcha']")).FindElement(By.TagName("img"));

                                Done("Got the captchaImg!");

                                TakesScreenshot(Browser, captchaImg, CAPTCHA_DIR);


                                Done("Captcha SAVED!");

                                RuCaptchaClient client = new RuCaptchaClient(API_KEY);
                                string captcha_id = client.UploadCaptchaFile(CAPTCHA_DIR);
                                string answer = null;
                                while (string.IsNullOrEmpty(answer))
                                {
                                    Thread.Sleep(5000);
                                    try
                                    {
                                        answer = client.GetCaptcha(captcha_id);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }

                                Done("Captcha ARRIVED : " + answer);

                                Browser.FindElement(By.Name("capcha")).SendKeys(answer);

                                Browser.FindElement(By.XPath(".//*[@data-blockid='btn'] ")).Click();



                                Warning("Registration complete!");

                                Done("E-MAIL : " + mail + "@mail.ru");
                                Done("Password : " + pass);

                                using (StreamWriter sw = new StreamWriter(ACCOUNTS_DIR, true, Encoding.Default))
                                {
                                    sw.WriteLine(mail + "@mail.ru|" + pass);
                                }

                                Thread.Sleep(2000);

                                Browser.Quit();

                                Browser = WebCreate(sr, proxy_add);
                                
                            }
                            else
                                Warning("Should 'START' first!");
                        }
                        break;
                    case "refresh":
                        Browser.Navigate().Refresh();
                        Done("Refreshed!");
                        break;
                    default:
                        if(command != "exit")
                            Warning("Undefined command");
                        break;
                }


            } while (command != "exit");

            Browser.Quit();

        }

        static void Done(string S)
        {
            Console.Write("Console >> ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(S);
            Console.ResetColor();
        }

        static void Warning(string S)
        {
            Console.Write("Console >> ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(S);
            Console.ResetColor();
        }

        static string randText(int length)
        {
            string S = "";
            var builder = new StringBuilder();
            var random = new Random();

            for (var i = 0; i < length; i++)
                builder.Append((char)random.Next('a', 'z' + 1)); // Случайная последовательность символов от A до Z.

            S = builder.ToString();

            return S;
        }

        static void TakesScreenshot(IWebDriver driver, IWebElement element, string CAPTCHA_DIR)
        {
            string fileName = CAPTCHA_DIR;
            Byte[] byteArray = ((ITakesScreenshot)driver).GetScreenshot().AsByteArray;
            Bitmap screenshot = new Bitmap(new System.IO.MemoryStream(byteArray));
            Rectangle croppedImage = new Rectangle(element.Location.X, element.Location.Y, element.Size.Width, element.Size.Height);
            screenshot = screenshot.Clone(croppedImage, screenshot.PixelFormat);
            screenshot.Save(String.Format(fileName, ImageFormat.Jpeg));
        }

        static IWebDriver WebCreate(StreamReader sr, bool proxy_add)
        {
            ChromeOptions options = new ChromeOptions();
            if (proxy_add)
            {
                string address = "";
                address = sr.ReadLine();
                var proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = address;
                proxy.SslProxy = "";
                options.Proxy = proxy;
            }
            options.AddArgument("ignore-certificate-errors");
            IWebDriver Browser = new ChromeDriver(options);
            Browser.Manage().Window.Maximize();
            Browser.Navigate().GoToUrl("https://account.mail.ru/signup/simple");
            Info();
            return Browser;
        }

        private static bool CanPing(string address)
        {
            Ping ping = new Ping();

            try
            {
                PingReply reply = ping.Send(address, 2000);
                if (reply == null) return false;

                return (reply.Status == IPStatus.Success);
            }
            catch (PingException e)
            {
                return false;
            }
        }

        static void Info()
        {
            //Console.Write("Console >> ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("Thanks for developing ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ilya Panko!");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("Follow me at ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("vk.com/loving_dolphin");
            Console.ResetColor();
        }
    }
}
